package com.freecharge.merchantonboarding.tests;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.MOBPathConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;

public class Update_Micro_Merchant_UseCases {
	
	@Test(enabled = true)
	public void update_micromerchant_integrationmode_Online_SuccessUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "lc6cmAJtceUsjd");
		updatemicromerchant_params.put("email", "testmerchanttest@gmail.com");
		updatemicromerchant_params.put("integrationMode", "ONLINE");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Successfully Updated the Micro Merchant (ONLINE)******************");
		/*Assert.assertNotNull(responseJson_updatemicromerchant.getString("merchantId"),"Merchant ID should not be displayed Null.");
		Assert.assertNotNull(responseJson_updatemicromerchant.getJSONObject("otherDetailsDTO").get("merchantStatus"), "Merchant Status should not be Null");*/
		
	}
	
	@Test(enabled = true)
	public void update_micromerchant_integrationmode_Offline_SuccessUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();


		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "AUDOWQFQWqQ0K2");
		updatemicromerchant_params.put("email", "aman@ccc.com");
		updatemicromerchant_params.put("integrationMode", "OFFLINE");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Successfully Updated the Micro Merchant (OFFLINE)******************");
		/*Assert.assertNotNull(responseJson_updatemicromerchant.getString("merchantId"),"Merchant ID should not be displayed Null.");
		Assert.assertNotNull(responseJson_updatemicromerchant.getJSONObject("otherDetailsDTO").get("merchantStatus"), "Merchant Status should not be Null");*/
		
	}
	
	@Test(enabled = true)
	public void update_micromerchant_integrationmode_FCPlus_SuccessUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Successfully Updated the Micro Merchant (FC+)******************");
		/*Assert.assertNotNull(responseJson_updatemicromerchant.getString("merchantId"),"Merchant ID should not be displayed Null.");
		Assert.assertNotNull(responseJson_updatemicromerchant.getJSONObject("otherDetailsDTO").get("merchantStatus"), "Merchant Status should not be Null");*/
		
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_invalid_imsid_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "abcinvalid");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with invalid IMS ID******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4190", "Error Code displayed when invalid IMS ID is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Merchant meta information can not be blank", "Message displayed when invalid IMS ID is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_imsid_FailureUseCase() throws Exception {
		
		RequestUtil requestUtil = new RequestUtil();

		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank IMS ID******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4187", "Error Code displayed when blank IMS ID is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Merchant IMS Id is blank", "Message displayed when blank IMS ID is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_invalid_integrationmode_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "INVALID");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with invalid Integration Mode******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4169", "Error Code displayed when invalid Integration Mode entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Invalid Integration Mode", "Message displayed when invalid Integration Mode entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_integrationmode_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank Integration Mode******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4166", "Error Code displayed when blank Integration Mode entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Integration mode cannot be left blank", "Message displayed when blank Integration Mode entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_merchantName_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank Merchant Name******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4146", "Error Code displayed when blank Merchant Name entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Merchant name cannot be left blank", "Message displayed when blank Merchant Name entered is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_merchantname_maxlimit_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		
		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "Test Merchant name more than maximum limit Test Merchant name more than maximum limit Test Merchant name more than maximum limit Test Merchant");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with Merchant Name more than max limit******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4147", "Error Code displayed when Merchant Name entered more than max limit is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Merchant name must be less than 127 characters", "Message displayed when Merchant Name entered more than max limit is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_without_merchantname_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "7896541230");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant without merchant name******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4146", "Error Code displayed when Merchant Name is not entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Merchant name cannot be left blank", "Message displayed when Merchant Name is not entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_invalid_mobileno_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "66");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with less than 10 digits mobile number******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4103", "Error Code displayed when less than 10 digits mobile number entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Please Enter valid Mobile Number", "Message displayed when less than 10 digits mobile number entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_morethan10_mobileno_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "96324458745211");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with more than 10 digits mobile number******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4103", "Error Code displayed when more than 10 digits mobile number entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Please Enter valid Mobile Number", "Message displayed when when more than 10 digits mobile number entered is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_mobileno_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank mobile number******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4109", "Error Code displayed when blank mobile number entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Mobile number can not be blank", "Message displayed when blank mobile number entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_duplicate_mobileno_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "Test address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with duplicate mobile numbe******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-5103", "Error Code displayed when duplicate mobile number entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"mobileNumber already Exists", "Message displayed when duplicate mobile number entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_address1_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank address1******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4148", "Error Code displayed when blank address1 entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Address cannot be left blank", "Message displayed when blank address1 entered is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_maxlimit_address1_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address updated for merchant new address updated for merchant New address updated for merchant New address updated for merchant New address updated for merchant New address updated for merchant New address updated for merchant New address updated for merchant New address updated for merchant");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with more than max limit address1******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4149", "Error Code displayed when address1 entered more than max limit is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Address must be less than 255 characters", "Message displayed when address1 entered more than max limit is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_city_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank city******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4117", "Error Code displayed when blank city entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"City cannot be left blank", "Message displayed when blank city entered is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_state_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "");
		businness_info_params.put("pincode", "560038");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank state******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4112", "Error Code displayed when blank state entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"State cannot be left blank", "Message displayed when blank state entered is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_blank_pincode_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with blank pincode******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4130", "Error Code displayed when blank pincode entered is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Pin code can not be blank", "Message displayed when blank pincode entered is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_morethan_6digit_pincode_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "560038955");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with more than 6 digits pincod******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4129", "Error Code displayed when pincode is entered more than 6 digits is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Pin code must be numeric and should be of six digits", "Message displayed when pincode is entered more than 6 digits is wrong");
				
	}
	
	
	@Test(enabled = true)
	public void update_micromerchant_with_lessthan_6digit_pincode_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "562");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with less than 6 digits pincode******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4129", "Error Code displayed when pincode is entered less than 6 digits is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Pin code must be numeric and should be of six digits", "Message displayed when pincode is entered less than 6 digits is wrong");
				
	}
	
	@Test(enabled = true)
	public void update_micromerchant_with_invalid_pincode_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();

		String RequstUrlCreateUpdatemicroMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_UPDATE_MICRO_MERCHANT_PATH;
		
		JSONObject updatemicromerchant_params = new JSONObject();
		JSONObject businness_info_params = new JSONObject();
		businness_info_params.put("merchantName", "TEST_HARSHA");
		businness_info_params.put("address1", "New address");
		businness_info_params.put("dateOfFormation", "08-02-2016");
		businness_info_params.put("TIN", "89208984578");
		businness_info_params.put("city", "Bangalore");
		businness_info_params.put("state", "Karnataka");
		businness_info_params.put("pincode", "abcde@@");
		businness_info_params.put("primaryMobile", "9765487542");
		
		updatemicromerchant_params.put("imsId", "w1V6p4ZZzJwisT");
		updatemicromerchant_params.put("email", "test18@test.com");
		updatemicromerchant_params.put("integrationMode", "FC+");
		updatemicromerchant_params.put("businessInformationDTO", businness_info_params);
		
				
		HttpResponse httpResponse_updatemicromerchant = requestUtil.postJSON_Request(RequstUrlCreateUpdatemicroMerchant, "freechargetest", updatemicromerchant_params);
		JSONObject responseJson_updatemicromerchant=requestUtil.getJSONObjectForResponse(httpResponse_updatemicromerchant);
		System.out.println("*************Updated the Micro Merchant with less than 6 digits pincode******************");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("errorCode"),"ER-4129", "Error Code displayed when invalid pincode is wrong");
		Assert.assertEquals(responseJson_updatemicromerchant.getString("message"),"Pin code must be numeric and should be of six digits", "Message displayed when invalid pincode is wrong");
				
	}
	
	
}
