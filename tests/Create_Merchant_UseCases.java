package com.freecharge.merchantonboarding.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.freecharge.core.MOBParamsGenerator;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.MOBPathConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;

public class Create_Merchant_UseCases {
	
	@Test(enabled = true)
	public void create_merchant_SuccessUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		//Util requtil = new Util();
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "testmerchant"+requestUtil.createRandomNumber(3)+"@gmail.com";
		
		JSONObject createmerchant_params = MOBParamsGenerator.create_merchant_Json(emailID, "FC+");
				
		HttpResponse httpResponse_createmerchant = requestUtil.postJSON_Request(RequstUrlCreateMerchant, "freechargetest", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Successfully Created the Merchant******************");
		Assert.assertNotNull(responseJson_createmerchant.getString("merchantId"),"Merchant ID should not be displayed Null.");
		Assert.assertEquals(responseJson_createmerchant.getString("email"),emailID, "Wrong Email ID is displayed.");
		Assert.assertNotNull(responseJson_createmerchant.getString("integrationMode"), "Integration ID should not be Null");
		Assert.assertNotNull(responseJson_createmerchant.getJSONObject("otherDetailsDTO").get("merchantStatus"), "Merchant Status should not be Null");
		
	}
	
	@Test(enabled = false)
	public void create_merchant_with_duplicate_emailID_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		Util requtil = new Util();
		
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "testmerchant692@gmail.com";
		
		List<NameValuePair> createmerchant_params = new ArrayList<NameValuePair>();
		createmerchant_params.add(new BasicNameValuePair("email", emailID));
				
		HttpResponse httpResponse_createmerchant = requtil.postJSON_Request(RequstUrlCreateMerchant, "freechargetest", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Create Merchant with Duplicate EmailID******************");
		Assert.assertEquals(responseJson_createmerchant.getString("errorCode"),"ER-5102", "Error Code displayed when duplicate email ID is wrong");
		Assert.assertEquals(responseJson_createmerchant.getString("message"),"Email already exists", "Message displayed when duplicate email ID wrong");
		
	}
	
	@Test(enabled = false)
	public void create_merchant_with_invalid_emailID_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		Util requtil = new Util();
		
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "testmerchant";
		
		List<NameValuePair> createmerchant_params = new ArrayList<NameValuePair>();
		createmerchant_params.add(new BasicNameValuePair("email", emailID));
				
		HttpResponse httpResponse_createmerchant = requtil.postJSON_Request(RequstUrlCreateMerchant, "freechargetest", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Create Merchant with invalid EmailID******************");
		Assert.assertEquals(responseJson_createmerchant.getString("errorCode"),"ER-4102", "Error Code displayed when invalid email ID is wrong");
		Assert.assertEquals(responseJson_createmerchant.getString("message"),"Please enter valid email address", "Message displayed when invalid email ID wrong");
		
	}
	
	@Test(enabled = false)
	public void create_merchant_with_blank_emailID_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		Util requtil = new Util();
		
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "";
		
		List<NameValuePair> createmerchant_params = new ArrayList<NameValuePair>();
		createmerchant_params.add(new BasicNameValuePair("email", emailID));
				
		HttpResponse httpResponse_createmerchant = requtil.postJSON_Request(RequstUrlCreateMerchant, "freechargetest", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Create Merchant with blank EmailID******************");
		Assert.assertEquals(responseJson_createmerchant.getString("errorCode"),"ER-4108", "Error Code displayed when blank email ID is wrong");
		Assert.assertEquals(responseJson_createmerchant.getString("message"),"Email can not be blank", "Message displayed when blank email ID wrong");
		
	}
	
	@Test(enabled = false)
	public void create_merchant_with_invalid_emailID_space_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		Util requtil = new Util();
		
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "testmerchant456@gmail.com ";
		
		List<NameValuePair> createmerchant_params = new ArrayList<NameValuePair>();
		createmerchant_params.add(new BasicNameValuePair("email", emailID));
				
		HttpResponse httpResponse_createmerchant = requtil.postJSON_Request(RequstUrlCreateMerchant, "freechargetest", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Create Merchant with Space in the EmailID*****************");
		Assert.assertEquals(responseJson_createmerchant.getString("errorCode"),"ER-4102", "Error Code displayed when invalid email ID with space is wrong");
		Assert.assertEquals(responseJson_createmerchant.getString("message"),"Please enter valid email address", "Message displayed when invalid email ID with space is wrong");
		
	}
	
	
	@Test(enabled = false)
	public void create_merchant_with_invalid_client_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		Util requtil = new Util();
		
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "testmerchant"+requestUtil.createRandomNumber(3)+"@gmail.com";
		
		List<NameValuePair> createmerchant_params = new ArrayList<NameValuePair>();
		createmerchant_params.add(new BasicNameValuePair("email", emailID));
				
		HttpResponse httpResponse_createmerchant = requtil.postJSON_Request(RequstUrlCreateMerchant, "freech", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Create Merchant with Invalid Client*****************");
		Assert.assertEquals(responseJson_createmerchant.getString("errorCode"),"ER-4102", "Error Code displayed when invalid email ID with space is wrong");
		Assert.assertEquals(responseJson_createmerchant.getString("message"),"Please enter valid email address", "Message displayed when invalid email ID with space is wrong");
		
	}
	
	@Test(enabled = false)
	public void create_merchant_with_blank_client_FailureUseCase() throws Exception {
		RequestUtil requestUtil = new RequestUtil();
		Util requtil = new Util();
		
		
		
		String RequstUrlCreateMerchant = Testing_Environment.Test_Environment+MOBPathConstants.CREATE_MERCHANT_PATH;
		String emailID = "testmerchant"+requestUtil.createRandomNumber(3)+"@gmail.com";
		
		List<NameValuePair> createmerchant_params = new ArrayList<NameValuePair>();
		createmerchant_params.add(new BasicNameValuePair("email", emailID));
				
		HttpResponse httpResponse_createmerchant = requtil.postJSON_Request(RequstUrlCreateMerchant, "", createmerchant_params);
		JSONObject responseJson_createmerchant=requestUtil.getJSONObjectForResponse(httpResponse_createmerchant);
		System.out.println("*************Create Merchant with Invalid Client*****************");
		Assert.assertEquals(responseJson_createmerchant.getString("errorCode"),"ER-4102", "Error Code displayed when invalid email ID with space is wrong");
		Assert.assertEquals(responseJson_createmerchant.getString("message"),"Please enter valid email address", "Message displayed when invalid email ID with space is wrong");
		
	}
	
}