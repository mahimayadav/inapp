package com.freecharge.core;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Wallet util class is used to hit the URl and all the util methods used for the Testing API.
 * @author pawangaria
 *
 */
public class Util {
	// headers
		private final String HEADER_USER_AGENT = "User-Agent";
		private final String HEADER_CONTENT_TYPE = "Content-Type";
		private final String HEADER_CONTENT_LENGTH = "Content-Length";
		private final String HEADER_MACHINE_IDENTIFIER="userMachineIdentifier";
		private final String HEADER_REQ_DATE = "Request-Date";
		private HttpClient httpClient;

		public HttpResponse getRequest(String restURL) throws ClientProtocolException, IOException {
			httpClient = HttpClients.createDefault();
			HttpGet request = new HttpGet(restURL);
			HttpResponse httpResponse = httpClient.execute(request);
			return httpResponse;
		}

		public HttpResponse getRequest_With_Jason_ContentType(String restURL) throws ClientProtocolException, IOException {
			httpClient = HttpClients.createDefault();
			HttpGet request = new HttpGet(restURL);
			request.setHeader("Content-Type", "application/json");
			HttpResponse httpResponse = httpClient.execute(request);
			return httpResponse;
		}
		public HttpResponse getRequest_With_Jason_ContentType(String restURL,String clientName ) throws ClientProtocolException, IOException {
			//httpClient = HttpClients.createDefault();
			HttpGet request = new HttpGet(restURL);
			request.setHeader("Content-Type", "application/json");
			request.setHeader("clientName", clientName);
			Header[] requestHeaders = request.getAllHeaders();
			System.out.println("Request Header for Post:");
			for (Header header : requestHeaders) {
				System.out.println(header.toString());
			}
			System.out.println("Hitting Get request:"+request);
			HttpResponse httpResponse = httpClient.execute(request);
			System.out.println("Response Header for Post:");
			Header[] headers = httpResponse.getAllHeaders();
			for (Header header : headers) {
				System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
			}
			return httpResponse;
		}
		public HttpResponse postRequest(String restURL,String token, List<NameValuePair> params) throws ClientProtocolException, IOException {
			System.out.println("--------------------------------------------");
			httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(restURL);
			httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
			httpPost.setHeader(HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded");
			httpPost.setHeader("token", token);
			
			/*if (csrfRequestIdentifier_Header != null) {
				System.out.println("Adding new header");
				httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
			}*/
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			Header[] requestHeaders = httpPost.getAllHeaders();
			System.out.println("Request Header for Post:");
			for (Header header : requestHeaders) {
				System.out.println(header.toString());
			}
			HttpResponse httpResponse = httpClient.execute(httpPost);
			//HttpEntity respEntity = httpResponse.getEntity();

			System.out.println(httpResponse.getStatusLine());
			System.out.println(httpResponse.getStatusLine().getStatusCode());
			
			// get all headers
			System.out.println("Response Header:");
			Header[] headers = httpResponse.getAllHeaders();
			for (Header header : headers) {
				System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
			}

			//String responseString = EntityUtils.toString(respEntity, "UTF-8");
			//System.out.println("Response Data/Body:");
			//System.out.println("Response:" + responseString);

			/*
			 * if (respEntity != null) { // EntityUtils to get the response content
			 * String content = EntityUtils.toString(respEntity);
			 * //System.out.println("Content:"+content); }
			 */
			
			

			return httpResponse;
		}
	    
		/**
		 * Post JSON request method is used to post the Request in the JSON format.
		 * @param restURL
		 * @param params
		 * @return
		 * @throws Exception
		 */
		public  HttpResponse postJSON_Request(String restURL,String clientName, List<NameValuePair> params) throws Exception {
			//PropertyConfigurator.configure("./log4j.properties");

			httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(restURL);
			httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
			httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
			httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
			httpPost.setHeader("clientName", clientName);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
			// Create a Json object for the params.
			JSONObject JSONObjectData = new JSONObject();

		    for (NameValuePair nameValuePair : params) {
		        try {
		            JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
		        } catch (JSONException e) {

		        }
		    }
		    System.out.println("Request Message:"+JSONObjectData.toString());
	        //Convert the JSON object to String Entity
		    StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
			httpPost.setEntity(JsonEntityObj);
			Header[] requestHeaders = httpPost.getAllHeaders();
			System.out.println("Request Header for Post:");
			for (Header header : requestHeaders) {
				System.out.println(header.toString());
			}
			HttpResponse httpResponse = httpClient.execute(httpPost);
			// HttpEntity respEntity = httpResponse.getEntity();

			System.out.println(httpResponse.getStatusLine());
			System.out.println(httpResponse.getStatusLine().getStatusCode());

			// get all headers
			System.out.println("Response Header:");
			Header[] headers = httpResponse.getAllHeaders();
			for (Header header : headers) {
				System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
			}

				return httpResponse;

		}
		
		
		
		
		public String getMimeType(HttpResponse httpResponse) throws ClientProtocolException, IOException {

			// Assert.assertEquals(expectedMimeType,ContentType.getOrDefault(httpResponse.getEntity()).getMimeType());
			return ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
		}

		public JSONObject getJSONObjectForResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException, JSONException {
			HttpEntity respEntity = httpResponse.getEntity();
			String responseString = EntityUtils.toString(respEntity, "UTF-8");
			// Convert the response to a String format
			System.out.println("Response Data/Body:");
			System.out.println("Response:" + responseString);
			// Convert the result as a String to a JSON object
			JSONObject jo = new JSONObject(responseString);

			return jo;

		}
	    
		/**
		 * Generates a unique reference id
		 * 
		 * @return
		 */
		public  String generateUUID() {
			// Long uuid = UUID.randomUUID().getMostSignificantBits();
			// String uid = String.valueOf(uuid);
			// uid = uid.substring(0, 10);
			DateFormat df = new SimpleDateFormat("ddMMyyHHmmss");
			Date dateobj = new Date();
			String uid = df.format(dateobj);
			// log.info(df.format(dateobj));
			return uid;
		}
		
		public String createRandomNumber(long len) {
		    if (len > 18)
		        throw new IllegalStateException("To many digits");
		    long tLen = (long) Math.pow(10, len - 1) * 9;

		    long number = (long) (Math.random() * tLen) + (long) Math.pow(10, len - 1) * 1;

		    String tVal = number + "";
		    if (tVal.length() != len) {
		        throw new IllegalStateException("The random number '" + tVal + "' is not '" + len + "' digits");
		    }
		    return tVal;
		}
	}
