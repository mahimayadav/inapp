package com.freecharge.ims.migration.core;

import java.io.File;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;

import com.freecharge.core.IMSpathConstants;
import com.freecharge.core.Testing_Environment;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogLevel;
import com.relevantcodes.extentreports.LogStatus;


/**
 * Assert Methods with Extent Reports
 * @author Sri harsha
 *
 */
public class Results {
	public static ExtentReports extreports = new ExtentReports(System.getProperty("user.dir")+File.separator+"Reports.html");
	public static ExtentTest log;
	
	
	public void createtestcase(String testcasename, String testname) throws Exception {
	
		log = extreports.startTest(testcasename);
		log.assignCategory(testname);
		extreports.endTest(log);
		
	}
	
	public void assertnotnull(Object obj, String message) throws Exception {
	
		
		try
		{
			Assert.assertNotNull(obj, message);
			log.log(LogStatus.PASS, message, "");
			
		}
		catch(AssertionError e)
		{
			log.log(LogStatus.FAIL, message, e);
			Assert.assertNotNull(obj, message);
		}
		finally
		{
			extreports.endTest(log);
			extreports.flush();
		}
	}
	
	public void assertequals(Object actual, Object expected, String message) {
	
		
		try
		{
			Assert.assertEquals(actual, expected, message);
			log.log(LogStatus.PASS, message, "");
			
		}
		catch(AssertionError e)
		{
			log.log(LogStatus.FAIL, message, e);
			Assert.assertEquals(actual, expected, message);
		}
		
		finally
		{
			extreports.endTest(log);
			extreports.flush();
		}
		
	}
	
	
	
	
}
