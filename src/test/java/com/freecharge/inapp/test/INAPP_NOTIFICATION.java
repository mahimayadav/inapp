package com.freecharge.inapp.identity.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.freecharge.core.Inapp;
import com.freecharge.core.MOBPathConstants;
import com.freecharge.core.Testing_Environment;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.snapdeal.mob.exception.ServiceException;
import com.snapdeal.mob.response.CreateQRCodeForMerchantResponse;

public class INAPP_NOTIFICATION {
	
	
	public String Starttime()
	{

		String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		char a[]=time.toCharArray();
		a[0]='2';
		a[1]='0';
		a[2]='1';
		a[3]='5';
		time=String.valueOf(a);
		System.out.println(time);
		return time;
	}
	public String Endtime()
	{

		String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		char a[]=time.toCharArray();
		a[5]='1';
		a[6]='2';
		time=String.valueOf(a);
		System.out.println(time);
		return time;
	}
	@Test(enabled  = true)
	public void INAPP_Notification_VALIDITY_Alert_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Validity Alert"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","9980377776"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 34"));
		Fieldmap_PARA.add(new BasicNameValuePair("field4","100 MB"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("field4_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","RECHARGE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Freecharge reminds when validity is about to expire"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&number=9980377777&type=prepaid&amount=34&operator=Airtel&operatorId=2&circle=Karnataka&circleId=10&source=message"));
		Fieldmap_PARA.add(new BasicNameValuePair("info1","Due on May 17"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/airtel.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Valdiity Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Prepaid Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","Validity Reminder"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","6"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","5"));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:number=9980377777&type=prepaid&amount=34&operator=Airtel&circle=Karnataka&source=message"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Airtel"));
		Fieldmap_PARA.add(new BasicNameValuePair("Amount","34"));
		Fieldmap_PARA.add(new BasicNameValuePair("DueDate","17 May"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&number=9999999999&productType=V&amount=30&operator=Airtel&source=message&circle=Delhi"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","6"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","1"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","validity"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_UTILITY_Alert_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Best Mumbai"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","6623491173"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 230"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","PAY NOW"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Freecharge reminds when your bill is due"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&type=billpay&productType=E&billerType=3&serviceProvider=202&serviceRegion=-1&additionalInfo1=6623491173&additionalInfo2=&additionalInfo3=&additionalInfo4=&additionalInfo5=&amount=675.00&fcBillerId=&circleName=&operatorName=BEST&operator=BEST&operatorLogo=images/circular-operator-logos/bills/bmestu.png&agBillId=&billDate=2015-10-06&dueDate=2015-10-20&billNumber=7871482000-06/10/2015&isPartial=false"));
		Fieldmap_PARA.add(new BasicNameValuePair("info1","Due on Oct 20"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/bills/bmestu.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Utility Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Bill Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","Utility Reminder"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","5"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","6"));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:type=billpay&serviceProvider=202&serviceRegion=-1&additionalInfo1=6623491173&additionalInfo2=&additionalInfo3=&additionalInfo4=&additionalInfo5=&amount=675.00&fcBillerId=&circleName=&operatorName=BEST&operatorLogo=images/circular-operator-logos/bills/bmestu.png&agBillId=&billDate=2015-10-06&dueDate=2015-10-20&billNumber=7871482000-06/10/2015&isPartial=false"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","BEST"));
		Fieldmap_PARA.add(new BasicNameValuePair("Amount","230"));
		Fieldmap_PARA.add(new BasicNameValuePair("DueDate","20 Oct"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&type=billpay&productType=E&billerType=3&serviceProvider=202&serviceRegion=-1&additionalInfo1=6623491173&additionalInfo2=&additionalInfo3=&additionalInfo4=&additionalInfo5=&amount=675.00&fcBillerId=&circleName=&operatorName=BEST&operator=BEST&operatorLogo=images/circular-operator-logos/bills/bmestu.png&agBillId=&billDate=2015-10-06&dueDate=2015-10-20&billNumber=7871482000-06/10/2015&isPartial=false"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","10"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","1"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","utility"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Prepaid_top_up_heuristics_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Balance Low?"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 50"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","RECHARGE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Recommended based on your past recharge trend"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidTopupHeuristicsInapp"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/airtel.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Heuristics based topup"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Prepaid Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","prepaidheuristics"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","68"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","7"));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidTopupHeuristicsAndroidPush"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Airtel"));
		Fieldmap_PARA.add(new BasicNameValuePair("RechargeNumber","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidTopupHeuristicsiOSPush"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","12"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","PrepaidTopupHeuristicsInapp"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Heuristics_for_low_data_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Data Low?"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 50"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","RECHARGE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Recommended based on your past recharge trend"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidDataHeuristicsInapp"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/airtel.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Heuristics based data"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Prepaid Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","prepaidheuristics"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","69"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","8"));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidDataHeuristicsAndroidPush"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Airtel"));
		Fieldmap_PARA.add(new BasicNameValuePair("RechargeNumber","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidDataHeuristicsiOSPush"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","13"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","PrepaidDataHeuristicsInapp"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Heuristics_postpaid_bill_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Bill Due?"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 1000"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","RECHARGE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Recommended based on your past bill pay trend"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=M&type=postpaid&amount=5000&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PostpaidHeuristicsInapp"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/airtel.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Heuristics based Postpaid"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Postpaid Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","PostpaidHeuristicsInapp"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","78"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","9"));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:number=9980377788&productType=M&type=postpaid&amount=5000&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PostpaidHeuristicsAndroidPush"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Airtel"));
		Fieldmap_PARA.add(new BasicNameValuePair("RechargeNumber","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=M&type=postpaid&amount=500&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PostpaidHeuristicsiOSPush"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","11"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","PostpaidHeuristicsInapp"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Postpaid_bill_pay_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Airtel Postpaid"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 500"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("info1","Due on Jul 18"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","RECHARGE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Based on SMS alerts. We don't read sensitive info."));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=M&type=postpaid&amount=500&rechargeType=&operator=Airtel%20Postpaid&circle=ALL&operatorId=12&circleId=1&source=PostpaidSMSBasedInApp"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/airtel.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Postpaid reminders basis SMS parsing"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","PostpaidSMSBased"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","PostpaidSMSBased"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","32"));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:number=9980377788&productType=M&type=postpaid&amount=500&rechargeType=&operator=Airtel%20Postpaid&circle=ALL&operatorId=12&circleId=1&source=PostpaidSMSBasedAndroidPush"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Airtel Postpaid"));
		Fieldmap_PARA.add(new BasicNameValuePair("RechargeNumber","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("Amount","500"));
		Fieldmap_PARA.add(new BasicNameValuePair("DueDate","october 18"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","11"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","PostpaidSMSbasedInapp"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Payment_retry_inapp_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Retry your Payment"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","8007861410"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs.10.0"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("info1","Expires at 01:05:38 hrs"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","PAY NOW"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Complete your previous payment attempt"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/vodafone.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Payment Retry"));
		Fieldmap_PARA.add(new BasicNameValuePair("name","Prashanth"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Payment Retry Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","Payment Retry"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","79"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","10"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		Fieldmap_PARA.add(new BasicNameValuePair("message","Dear Avinash, We noticed that your transaction of Rs.10.0 for Vodafone Recharge wasn't completed. We have saved your details. Just tap here and complete now."));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Vodafone"));
		Fieldmap_PARA.add(new BasicNameValuePair("RechargeNumber","9980377788"));
		Fieldmap_PARA.add(new BasicNameValuePair("Amount","10.0"));
		Fieldmap_PARA.add(new BasicNameValuePair("DueDate","october 18"));
		Fieldmap_PARA.add(new BasicNameValuePair("operatorName","Vodafone"));
		Fieldmap_PARA.add(new BasicNameValuePair("deeplink","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","23"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","1"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","payment_retry"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Auto_enrollment_heuristics_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Save new biller"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","BESCOM"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","82913170001"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("info1","Never miss deadlines"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","REMEMBER"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","We'll remind you when the bill is due for FREE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url",""));
	//	Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/vodafone.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Auto Enrollment"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("name","Prashanth"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Auto Enrollment"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","AutoEnrollmentInapp"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id",""));
	//	Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","10"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("message","Dear Avinash, We noticed that your transaction of Rs.10.0 for Vodafone Recharge wasn't completed. We have saved your details. Just tap here and complete now."));
	//	Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("Operator","Vodafone"));
		Fieldmap_PARA.add(new BasicNameValuePair("Number","82913170001"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("Amount","10.0"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("DueDate","october 18"));
		Fieldmap_PARA.add(new BasicNameValuePair("operator","BESCOM"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("deeplink","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","9"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","AutoEnrollmentInapp"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Request_money_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Anant"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","Requested"));
		Fieldmap_PARA.add(new BasicNameValuePair("field2","Rs.230"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","5:47 PM"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field2_color","#E3714D"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#878787"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("info1","Never miss deadlines"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","ACCEPT"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","REJECT"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Freecharge lets friends send and request money"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=chat&type=requestmoney&handle=PHONE&handleIdentifier=9711346390"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/bills/bmestu.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Request Money Alert"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("name","Prashanth"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Request Money Alert"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","Request Money Alert"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id",""));
	//	Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","10"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("message","Dear Avinash, We noticed that your transaction of Rs.10.0 for Vodafone Recharge wasn't completed. We have saved your details. Just tap here and complete now."));
	//	Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("Operator","Vodafone"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("Number","82913170001"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("Amount","10.0"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("DueDate","october 18"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("operator","BESCOM"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("deeplink","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","8"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","Request Money Alert"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}
	
	@Test(enabled  = true)
	public void INAPP_Notification_Low_data_alert_SuccesssUsecase() throws Exception {
		//String captureLcnt_Merchant_lcnServ = WalletEnvironment.Test_Environment2 + GeoLocationPathConstants.MERCHANT_LOCATION_PATH;
		String INAPP_NOTIFICATION_PATH = Testing_Environment.Test_Environment_location + Inapp.inapp_notificaition;

	    RequestUtil requestUtil = new RequestUtil();
		//JSONObject responseJson_userCreated = IMSUtil.createNewUserOnServer(requestUtil);
		List<NameValuePair> Fieldmap_PARA = new ArrayList<NameValuePair>();
		Fieldmap_PARA.add(new BasicNameValuePair("title","Data Low?"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1","9980377788"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("field2","Rs.230"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3","Rs. 50"));
		Fieldmap_PARA.add(new BasicNameValuePair("field1_color","#878787"));
		Fieldmap_PARA.add(new BasicNameValuePair("field3_color","#E3714D"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("info1","Never miss deadlines"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1","RECHARGE"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta2","DISMISS"));
		Fieldmap_PARA.add(new BasicNameValuePair("justification_text","Recommended based on your past recharge trend"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_type","DEEPLINK"));
		Fieldmap_PARA.add(new BasicNameValuePair("cta1_action_url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidDataHeuristicsInapp"));
		Fieldmap_PARA.add(new BasicNameValuePair("image_url","images/circular-operator-logos/airtel.png"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_use_case","Heuristics based data"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("name","Prashanth"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_category","Prepaid Reminders"));
		Fieldmap_PARA.add(new BasicNameValuePair("meta_data_tag","prepaidheuristics"));
		Fieldmap_PARA.add(new BasicNameValuePair("android_push_notification_template_id","69"));
		Fieldmap_PARA.add(new BasicNameValuePair("apple_push_notification_template_id","8"));
		Fieldmap_PARA.add(new BasicNameValuePair("url","freechargeapp://freecharge?action=recharge&number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidDataHeuristicsiOSPush"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("message","Dear Avinash, We noticed that your transaction of Rs.10.0 for Vodafone Recharge wasn't completed. We have saved your details. Just tap here and complete now."));
		Fieldmap_PARA.add(new BasicNameValuePair("messageUrl","recharge:number=9980377788&productType=V&type=prepaid&amount=30&rechargeType=&operator=Airtel&circle=Karnataka&operatorId=2&circleId=10&source=PrepaidDataHeuristicsAndroidPush"));
		Fieldmap_PARA.add(new BasicNameValuePair("Operator","Airtel"));
		Fieldmap_PARA.add(new BasicNameValuePair("RechargeNumber","9980377788"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("Amount","10.0"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("DueDate","october 18"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("operator","BESCOM"));
	//	Fieldmap_PARA.add(new BasicNameValuePair("deeplink","freechargeapp://freecharge?action=recharge&type=prepaid&number=8007861410&amount=10.0&operator=Vodafone&operatorId=14&circleId=14&orderid=FCVA160906464007700&UTM_source=pay_retry_notif_android_push"));
		
		String endtime= Endtime();
		String starttime= Starttime();
		List<NameValuePair> INAPP_NOTIFICATION_PARA = new ArrayList<NameValuePair>();
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("userId","130446280"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("channelIdList","3"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("templateId","13"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("fieldMap",Fieldmap_PARA.toString()));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("startTime",starttime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("endTime",endtime));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("isPushEnabled","0"));
		INAPP_NOTIFICATION_PARA.add(new BasicNameValuePair("source","PrepaidDataHeuristicsInapp"));
	
		// Post request to the URL with the param data
		HttpResponse httpResponseinapp = requestUtil.postJSON_Request_inapp(INAPP_NOTIFICATION_PATH, INAPP_NOTIFICATION_PARA);
		JSONObject responseJson_inapp=requestUtil.getJSONObjectForResponse(httpResponseinapp);
		System.out.println("***************************Inapp notification*************************************************************");
		Assert.assertNotNull(responseJson_inapp.getInt(""),"inapp should not be Null");
		
	}

	
}